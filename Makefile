JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64
CXXFGAGS += -I$(JAVA_HOME)/include 
CXXFGAGS += -I$(JAVA_HOME)/include/linux


all: build

generate-header:
	echo prepare...
	mkdir -p jniexperiment/bin
	javac -d jniexperiment/bin/ jniexperiment/src/main/java/jnitest/Utils.java
	cd jniexperiment/bin/ && javah jnitest.Utils

build-native:
	g++ $(CXXFGAGS) -fPIC jniexperiment/bin/jnitest_Utils.cpp -shared -o helloworld.so -Wl,-soname -Wl,--no-whole-archive

build: build-native
	cd jniexperiment && mvn install

clean:
	rm -rf jniexperiment/target

